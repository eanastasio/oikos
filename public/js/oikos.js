var OikosUI = (function(jQuery, window, document){

	var OikosUI = function(){ };

	var setUISuccessMsg = function (msg){
		alert("funciona " + msg);
	};

	OikosUI.prototype = {
		setUISuccessMsg:setUISuccessMsg		
	};

	return OikosUI;
});


var OikosController = (function(jQuery, OikosUI, window, document){
	var EXP_PER_POST;
	var main = function(){
		bindersExpenses();
		EXP_PER_POST = 15;
		page_active();
	};

	var page_active = function(){
		var page = $(".page_active").val();
		switch(page){
			case "1":
				$(".expense").addClass("active");
				$(".entry").removeClass("active");
			break;
			case "2":
				$(".entry").addClass("active");
				$(".expense").removeClass("active");
			break;
			default:
				$(".expense").addClass("active");
				$(".entry").removeClass("active");
			break;
		}
	};

	var bindersExpenses = function(){ 
		setFormatToDatePickerUI();

		$("#btnSubmit").click(submitExpenses);
		$("#btnSubmitSrch").click(searchExpenses);
		$("#pager_expense li a").click(getExpensesForPage); 
	};

	var setFormatToDatePickerUI = function(){
		$.datepicker.setDefaults($.datepicker.regional["es"]);
		$("#date-expense").datepicker({
			autoSize: true,
			dateFormat: "yy-mm-dd",
			onSelect: function (date) {
				var d = new Date();
				date = date + ' ' + d.getHours() +':'+ d.getMinutes() +':'+ d.getSeconds();
				$("#date-expense").val(date); 
			}
		});

		$("#start_date").datepicker({
			autoSize: true,
			dateFormat: "yy-mm-dd",
			onSelect: function(date){
				var min = date;
				$("#end_date").datepicker("option", "disabled", false);
				$("#end_date").datepicker("option", "minDate", min);
			}
		});
		$("#end_date").datepicker({
			autoSize: true,
			dateFormat: "yy-mm-dd"
		});
		$("#end_date").datepicker("option", "disabled", true);

	};

	var searchExpenses = function(){
		var min = $("#start_date").val();
		var max = $("#end_date").val();
		min = JSON.stringify(min);
		max = JSON.stringify(max);
		console.log("minimo y maximo " + min +" "+ max);
		$.ajax({
			url: 'search',
			type: 'post',
			cache: false,
			dataType: 'json',
			data: {min : min, max : max },
			success: function(data){
				console.log("Va todo bien "  + data);
				$.each(data, function(i, v){
					console.log("Datos: " + data[i]['id'] + data[i]['name'] + data[i]['name_cat'] + data[i]['date_expense'] + data[i]['amount']);
				});
			},
			error: function(xhr, textStatus, thrownError){
				alert('Error... alguien será azotado por esto ;)');
			}
		});
	};

	//getExpenses() in the controller
	var submitExpenses = function(e){
		e.preventDefault();
		$.ajax({
			url: 'add',
            type: 'post',
            cache: false,
            dataType: 'json',
            data: $('form#expenseForm').serialize(),
            success: function(data) {
       	        //oikosUI.setUISuccessMsg(data.msg);
       	        setUISuccessMsg(data.msg);
       	        emptyFields();
       	        loadTableWithExpenses();
            },
            error: function(xhr, textStatus, thrownError) {
                alert('Algo salio mal :(...');
            }
        });
	};

	var loadTableWithExpenses = function(){
		$.ajax({
			url: 'getAll',
            type: 'post',
            cache: false,
            dataType: 'json',
            success: function(data) {
            	loadtTableUI(data);
            },
            error: function(xhr, textStatus, thrownError) {
                alert('Algo salio mal :(...');
            }
        });
	};

	var emptyFields = function(){
		$('#date-expense').val('');
		$('#amount').val('');
	};

	var loadtTableUI = function(expenses){
		var wrapper = $('#tbl_expenses > tbody');
		wrapper.fadeOut('fast');
		wrapper.html('');
		$.each(expenses, function(i, v){
			wrapper.append('<tr class="'+ expenses[i]['id']+'"><td>' + expenses[i]['name'] + '</td><td>' + expenses[i]['name_cat'] + 
				'</td> <td>' + expenses[i]['date_expense'] + '</td><td>' + expenses[i]['amount'] + '</td></tr>');
		});
		wrapper.fadeIn('slow');
	};

	var setUISuccessMsg = function(msg){
		var wrapper = $("#msg_div");
		wrapper.addClass('alert-success');
		wrapper.find("p").text( msg );
		wrapper.fadeIn('fast');
		wrapper.delay(3000).fadeOut('slow');
	};

	var getExpensesForPage = function(){
		var page = $(this).text();
		var skp = (page - 1) * EXP_PER_POST;
		console.log("El valor que se va a mandar es " +  skp);
		$.ajax({
			url: 'getAll',
            type: 'post',
            cache: false,
            data: {skip : skp},
            dataType: 'json',
            success: function(data) {
            	console.log("Si jala! " + data.length);
            	loadtTableUI(data);
            },
            error: function(xhr, textStatus, thrownError) {
                alert('Algo salio mal en la paginación :(...');
            }
        });
	}; 

	var oikosUI = new OikosUI();
	var OikosController = function(){ };

	OikosController.prototype = {
		main:main		
	};

	return OikosController;

})(jQuery, OikosUI, window, document);

var oikosController = new OikosController();
$(oikosController.main);