<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['before' => 'guest', function()
{
	return View::make('start');
}]);

Route::get('admin/users', function(){
	return "Vamos a administrar usuarios";
});

Route::get('auth/login', ['before' => 'guest', function(){
	return View::make('auth/login');
}]);

//Route::get('expense/index', ['before' => 'auth', function(){
//	return View::make('expense/index_expense');
//}]);

/**Rutas para el modulo de entradas**/
Route::get('entry/index', ['before' => 'auth', function(){
	return View::make('entry/index_entry');
}]);

/**Rutas para las acciones del modulo de gastos**/
Route::get('expense/index', ['uses' => 'ExpenseController@doExpenseUI', 'before' => 'auth']);
Route::post('expense/add', ['uses' => 'ExpenseController@addExpense', 'before' => 'auth']);
Route::post('expense/search', ['uses' => 'ExpenseController@searchExpenses', 'before' => 'auth']);
Route::post('expense/getAll', ['uses' => 'ExpenseController@getExpenses', 'before' => 'auth']);
//Route::post('expense/srchExpenses', ['uses' => 'ExpenseController@searchExpenses', 'before' => 'auth']);

/**Rutas para el modulo de usuarios**/
Route::post('/login', ['uses' => 'AuthController@doLogin', 'before' => 'guest']);
Route::get('/logout', ['uses' => 'AuthController@doLogout', 'before' => 'auth']);
//Usuarios 
Route::post('/users/add', ['uses' => 'UserController@addUser', 'before' => 'guest']);