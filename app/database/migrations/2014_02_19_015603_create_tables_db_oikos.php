<?php

use Illuminate\Database\Migrations\Migration;

class CreateTablesDbOikos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('period', function($table){
			$table->increments('id');
			$table->string('name', 60);
			$table->dateTime('start');
			$table->dateTime('end');

			$table->timestamps();
		});

		Schema::create('entry_category', function($table){
			$table->increments('id');
			$table->string('name', 60);
			$table->integer('user_id')->unsigned();

			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users');
		});
		
		Schema::create('expense_category', function($table){
			$table->increments('id');
			$table->string('name', 60);
			$table->integer('user_id')->unsigned();
			
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users');
		});		

		Schema::create('expense', function($table){
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('period_id')->unsigned();
			$table->integer('expense_category_id')->unsigned();
			$table->dateTime('date_expense');
			$table->decimal('amount');

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('period_id')->references('id')->on('period');
			$table->foreign('expense_category_id')->references('id')->on('expense_category');
		});

		Schema::create('entry', function($table){
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('period_id')->unsigned();
			$table->integer('entry_category_id')->unsigned();
			$table->dateTime('date_entry');
			$table->decimal('amount');

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('period_id')->references('id')->on('period');
			$table->foreign('entry_category_id')->references('id')->on('entry_category');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExist('period');
		Schema::dropIfExist('expense_category');
		Schema::dropIfExist('entry_category');

		Schema::dropIfExist('expense');
		Schema::dropIfExist('entry');
	}

}