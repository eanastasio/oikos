<?php 

class UserTableSeeder extends Seeder{
	public function run(){
		$users = [
			['nombre' => 'test', 'apaterno' => 'testp', 'amaterno' => 'testa', 'email' => 'mail@mail.com', 'password' => Hash::make('1234')]
		];

		DB::table('users')->insert($users);
	}
}