<?php

class Entry extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'entry';

	//Relacion con usuarios
	public function user(){
		return $this->belongsTo('User');
	}

	//Relacion con period
	public function period(){
		return $this->belongsTo('Period');
	}

	//Relacion con EntryCategory
	public function entryCategory(){
		return $this->belongsTo('EntryCategory');
	}
}