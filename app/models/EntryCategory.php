<?php 

class EntryCategory extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'entry_category';

	public function entries(){
		return $this->hasMany('Entry');
	}

	//Relacion con usuarios
	public function user(){
		return $this->belongsTo('User');
	}
}