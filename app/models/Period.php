<?php

class Period extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'period';

	public function entries()
	{
		return $this->hasMany('Entry');
	}

	public function expenses(){
		return $this->hasMany('Expense');
	}

	public function periodsByUser(){
		$periods = Period::all();
		return $periods;
	}
	
}