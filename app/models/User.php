<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	public $errors;

	protected $fillable = array('nombre', 'apaterno', 'amaterno', 'email', 'password');
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function isValid($data){
		$rules = array(
			'email' => 'required|email|unique:users',
			'nombre' => 'required|min:4|max:40',
			'password' => 'required|min:6|confirmed'
		);

		$validator = Validator::make($data, $rules);

		if($validator->passes()){
			return true;
		}

		$this->errors = $validator->errors();
        return false;
	}

	public function entries(){
		return $this->hasMany('Entry');
	}

	public function expenses(){
		return $this->hasMany('Expense');
	}

	public function expensesCategories(){
		return $this->hasMany('ExpenseCategory');
	}

	public function entriesCategories(){
		return $this->hasMany('EntryCategory');
	}
}