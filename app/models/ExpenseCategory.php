<?php
class ExpenseCategory extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'expense_category';

	protected $fillable = array('name');

	public function expenses(){
		return $this->hasMany('Expense');
	}

	//Relacion con usuarios
	public function user(){
		return $this->belongsTo('User');
	}

	public function categoriesByUser($idUsr){
        $category = ExpenseCategory::select('name', 'id')->where('user_id', '=', $idUsr)->get()->lists('name', 'id'); //find(1);
        return $category;
	}

}