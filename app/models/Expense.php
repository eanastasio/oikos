<?php

class Expense extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'expense';

	public $POST_PER_PAGE; 

	public $errors;

	protected $fillable = array('user_id', 'period_id', 'expense_category_id', 'date_expense', 'amount');

	//Relacion con usuarios
	public function user(){
		return $this->belongsTo('User');
	}

	//Relacion con period
	public function period(){
		return $this->belongsTo('Period');
	}

	//Relacion con expenseCategory
	public function expenseCategory(){
		return $this->belongsTo('ExpenseCategory');
	}

	public function isValid($data){
		$rules = array(
			'user_id' => 'required',
			'expense_category_id' => 'required',
			'date_expense' => 'required',
			'amount' => 'required'
		);

		$validator = Validator::make($data, $rules);

		if($validator->passes()){
			return true;
		}

		$this->errors = $validator->errors();
        return false;
	}

	//obtine de la BD los gastos del $idUsr y un cortador $skip
	public function getMyExpenses($idUsr, $skip){
		$POST_PER_PAGE = 15;
		$expenses = DB::table('expense')
            ->join('expense_category', 'expense_category.id', '=', 'expense.expense_category_id')
            ->join('period', 'period.id', '=', 'expense.period_id')
            ->select('expense.id', 'expense.date_expense', 'expense.amount', 'expense_category.name as name_cat', 'period.name')
            ->where('expense.user_id', '=', $idUsr)->orderBy('expense.id', 'asc')->take($POST_PER_PAGE)->skip($skip)->get();

        return $expenses;
	}

	//Revizar como hacer el query y como no afectar el paginado 
	public function getMyExpensesSrch($idUsr, $min, $max){  //, $skip){
		$POST_PER_PAGE = 15;
		$expenses = DB::table('expense')
            ->join('expense_category', 'expense_category.id', '=', 'expense.expense_category_id')
            ->join('period', 'period.id', '=', 'expense.period_id')
            ->select('expense.id', 'expense.date_expense', 'expense.amount', 'expense_category.name as name_cat', 'period.name')
            ->where('expense.user_id', '=', $idUsr)
            ->whereBetween('expense.date_expense', array('2014-08-01', '2014-08-18'))->orderBy('expense.id', 'asc')->take($POST_PER_PAGE)->skip(0)->get();

        return $expenses;
	}

	//Obtiene el numero de paginas en base al numero de ingresos que haya en la BD, dividados en 15
	public function getPages($idUsr){
		$POST_PER_PAGE = 15;
		$pages =  DB::table('expense')
            ->join('expense_category', 'expense_category.id', '=', 'expense.expense_category_id')
            ->join('period', 'period.id', '=', 'expense.period_id')
            ->where('expense.user_id', '=', $idUsr)->count();
        return ceil($pages / $POST_PER_PAGE);
	}

}