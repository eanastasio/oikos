@extends('layout_tools')


@section('container')
  <input type="hidden" class="page_active" value="1" />
	<div class="page-header">
     <h1>Registro de gastos</h1>
	</div>
  <div class="row">
    <div class="col-md-4">
       @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Algo salio mal :(</strong>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
            </div>
      @endif
      @if(Session::get('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>:)</strong>
            {{ Session::get('success') }}
        </div>
      @endif
      {{Form::open(array('url'=>'expense/add', 'method' => 'POST', 'class' => 'form-signin', 'id' => 'expenseForm'), array('role'=>'form'))}}
        <h2 class="form-signin-heading">Cuanto gastamos?</h2>
          {{Form::select('period_id', $data['period'], null, array('class' => 'form-control'))}}
          {{Form::select('expense_category_id', $data['categories'], null, array('class' => 'form-control'))}}
          {{Form::text('date_expense', null, array('placeholder' => 'Fecha', 'class' => 'form-control', 'id' => 'date-expense'))}}
          {{Form::text('amount', null, array('placeholder' => '¿cuanto fue?', 'class' => 'form-control', 'id' => 'amount'))}}
          {{Form::hidden('user_id', Auth::user()->id)}}
        <br />
        {{Form::button('Registrar gasto', array('class'=>'btn btn-lg btn-primary btn-block', 'id' => 'btnSubmit'))}}
      {{Form::close()}}
       <br />
       <div id="msg_div" class="alert">
          <p></p>
      </div>
    </div>
    <div class="col-md-8">

      <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <p class="navbar-text">Filtrar por:</p>
          </div>
           <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              {{Form::open(array('url' => 'expense/search', 'method' => 'POST', 'class' => 'navbar-form navbar-left'), array('role' => 'search'))}}
                  <input type="text" id="start_date" class="form-control input_ok" placeholder="Fecha incial" />
                  <input type="text" id="end_date" class="form-control input_ok" placeholder="Fecha final" />
                {{Form::button('Buscar', array('class' => 'btn btn-primary', 'id' => 'btnSubmitSrch'))}}
              {{Form::close()}}
            </div><!-- /.navbar-collapse -->
        </div>
      </nav>
      <table class="table table-hover" id="tbl_expenses">
      <thead>
        <tr>
          <th>Periodo</th>
          <th>Categoría</th>
          <th>Fecha</th>
          <th>Monto</th>
        </tr>
      </thead>  
      <tbody>
        @if(isset($data['expense']))
          @foreach($data['expense'] as $expense)
          <tr class="{{$expense->id}}">
            <td>{{$expense->name}}</td>
            <td>{{ $expense->name_cat }}</td>
            <td>{{$expense->date_expense}}</td>
            <td>{{$expense->amount}}</td>
           </tr>
          @endforeach 
        @endif
      </tbody>    
    </table>
    <ul class="pagination" id="pager_expense">
       @if(isset($data['pages']) && $data['pages'] > 0)
        @for($i = 1; $data['pages'] >= $i; $i++)
          <li><a href="#" class="page_{{$i}}">{{$i}}</a></li>
        @endfor
      @endif
    </ul>
    </div>
  </div>
@stop