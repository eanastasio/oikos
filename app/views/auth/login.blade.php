@extends('auth.layout_sigin')

@section('sigin')
      @if ($errors->any())
            <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Por favor corrige los siguentes errores:</strong>
                  <ul>
                  @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                  @endforeach
                  </ul>
            </div>
      @endif
      @if(Session::get('msg_succeces'))
            <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <strong>¡Bienvenido!</strong> {{ Session::get('msg_succeces') }}
            </div>
      @endif
      {{Form::open(array('url'=>'/users/add', 'method' => 'POST', 'class' => 'form-signin'), array('role'=>'form'))}}
      	<h2 class="form-signin-heading">Nos presentamos</h2>
      	{{Form::text('nombre', null, array('placeholder' => '¿como te llamas?', 'class' => 'form-control'))}}
            {{Form::text('apaterno', null, array('placeholder' => '¿tu primer apellido?', 'class' => 'form-control'))}}
            {{Form::text('amaterno', null, array('placeholder' => '¿el segundo?', 'class' => 'form-control'))}}
      	{{Form::email('email', null, array('placeholder' => '¿tu email?', 'class' => 'form-control'))}}
      	{{Form::password('password', array('placeholder' => 'ahora el pass', 'class' => 'form-control'))}}
      	{{Form::password('password_confirmation', array('placeholder' => 'lo repites', 'class' => 'form-control'))}}

      	{{Form::submit('Registrarme', array('class'=>'btn btn-lg btn-primary btn-block'))}}
      {{Form::close()}}
@stop

@section('login')
      {{Form::open(array('url'=>'/login', 'method' => 'POST', 'class' => 'form-signin'), array('role'=>'form'))}}
      	<h2 class="form-signin-heading">¿Nos conocemos?</h2>      
            @if(Session::get('msg'))
                  <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Error:</strong>
                        <p>{{ Session::get('msg') }}</p>
                  </div>
            @endif
      	{{Form::text('email', null, array('placeholder' => 'Tu email', 'class' => 'form-control'))}}
      	{{Form::password('password', array('placeholder' => 'Ahora el pass', 'class' => 'form-control'))}}

      	{{--Form::label('recordar_user', 'Recordarme', array('class' => 'checkbox'))--}}
      	{{--Form::checkbox('recordar_user', 1, true)--}}
      	{{Form::submit('Entrar', array('class'=>'btn btn-lg btn-primary btn-block'))}}
      {{Form::close()}}
@stop