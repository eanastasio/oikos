@extends('layout_tools')


@section('container')
  <input type="hidden" class="page_active" value="2" />
	<div class="page-header">
     <h1>Registro de ingresos</h1>
	</div>
  <div class="row">
    <div class="col-md-4">
      {{Form::open(array('url'=>'', 'method' => 'POST', 'class' => 'form-signin'), array('role'=>'form'))}}
        <h2 class="form-signin-heading">¿Cuanto ganamos?</h2>
        <select id="periodo" class="form-control">
          <option value="0" selected="selected">Selecciona un periodo</option>
          <option value="mes">Mensual</option>
          <option value="quincenal">Quincenal</option>
        </select>
        <select id="categoria" class="form-control">
          <option value="0" selected="selected">Selecciona una categoría</option>
          <option value="mes">Mensual</option>
          <option value="quincenal">Quincenal</option>
        </select>
        {{Form::text('fecha', null, array('placeholder' => 'Fecha', 'class' => 'form-control'))}}
        {{Form::text('monto', null, array('placeholder' => '¿cuanto fue?', 'class' => 'form-control'))}}
        <br />
        {{Form::submit('Registrar ingreso', array('class'=>'btn btn-lg btn-primary btn-block'))}}
      {{Form::close()}}
    </div>
    <div class="col-md-8">
      <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <p class="navbar-text">Filtrar por:</p>
          </div>
           <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              {{Form::open(array('url' => '', 'method' => 'POST', 'class' => 'navbar-form navbar-left'), array('role' => 'search'))}}
                <div class="form-group">
                  <select id="periodo_srch" class="form-control">
                      <option value="0" selected="selected">Periodo</option>
                      <option value="mes">Mensual</option>
                      <option value="quincenal">Quincenal</option>
                  </select>
                  <select id="categoria_srch" class="form-control">
                      <option value="0" selected="selected">Categoría</option>
                      <option value="mes">Mensual</option>
                      <option value="quincenal">Quincenal</option>
                  </select>
                  <input type="text" class="form-control input_ok" placeholder="Fecha incial" />
                  <input type="text" class="form-control input_ok" placeholder="Fecha final" />
                </div>
                {{Form::submit('Buscar', array('class' => 'btn btn-primary'))}}
              {{Form::close()}}
            </div><!-- /.navbar-collapse -->
        </div>
      </nav>
       <table class="table">
      <thead>
        <tr>
          <th>Periodo</th>
          <th>Categoría</th>
          <th>Fecha</th>
          <th>Monto</th>
        </tr>
      </thead>  
      <tbody>
        <tr>
          <td>Mensual</td>
          <td>Transporte</td>
          <td>18/01/14</td>
          <td>$200</td>
        </tr>
        <tr>
          <td>Mensual</td>
          <td>Transporte</td>
          <td>18/01/14</td>
          <td>$200</td>
        </tr>
      </tbody>    
    </table>
    </div>
  </div>
@stop