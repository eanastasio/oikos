@extends('layout')

@section('loginbar')
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{URL::to('/')}}">Oikos Web</a>
        </div>
        <div class="navbar-collapse collapse">
          {{Form::open(array('url'=>'/login', 'method' => 'POST', 'class' => 'navbar-form navbar-right'),  array('role'=>'form'))}}
            <div class="form-group">
              <input type="text" name="email" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" name="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Entrar</button>
           {{Form::close()}}
        </div><!--/.navbar-collapse -->
      </div>
    </div>
@stop

@section('welcome')
      <div class="container">
        <h1>Bienvenido a Oikos!</h1>
        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
        <p><a class="btn btn-primary btn-lg" href="{{URL::to('auth/login')}}" role="button">Quiero registrarme &raquo;</a></p>
      </div>
@stop

@section('content')
<!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Administra</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">Ver mas &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Ahorra</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">Ver mas &raquo;</a></p>
       </div>
        <div class="col-md-4">
          <h2>Controla</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">Ver mas &raquo;</a></p>
        </div>
      </div>

      <hr>
      <footer>
          <p>&copy; Oikos 2014 </p>
      </footer>
@stop
