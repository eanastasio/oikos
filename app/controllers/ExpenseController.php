<?php 
/**Este Controlador se encarga administrar las funciones para llevar el control de los gastos**/
/*
Las funciones son
#doExpenseUI
#getExpenses
#addExpense
#searchExpenses
*/
class ExpenseController extends BaseController{

	//Se encarga de traer los gastos de la BD del usuario logeado
	public function doExpenseUI(){
		$expesesCategories = new ExpenseCategory;
		$idUsr = Auth::user()->id;
		$categories = $expesesCategories->categoriesByUser($idUsr);
		$period = Period::get()->lists('name', 'id');
		$expense = new Expense;
		$expenses = $expense->getMyExpenses($idUsr, 0);
		$pages = $expense->getPages($idUsr);
		$data = array('categories' => $categories , 'period' => $period, 'expense' => $expenses, 'pages' => $pages );
		return View::make('expense/index_expense')->with('data', $data);
	}

	//Obtiene los 15 (skip) gastos del usuario logeado
	public function getExpenses(){
		if (Input::has('skip'))
		{
			$skip = Input::get('skip');
			$expense = new Expense;
			$expenses = $expense->getMyExpenses(Auth::user()->id, $skip);
			if (Request::ajax())
				return Response::json($expenses);
			return 
				Response::json(array ('success' => false, 'msg' => 'Hubo un error, tenemos unos monos revizando el problema'));		
		}

		return Response::json(array ('success' => false, 'msg' => 'Hubo un error, no hay valor' ));		
	}

	//Agregar gasto a la base de datos
	public function addExpense(){
		$expense = new Expense;
		$data = Input::all();
		$isAjax = Request::ajax();

		if ($expense->isValid($data)) {
			$expense->fill($data);
			$expense->save();
			if($isAjax) {
            	return Response::json(array (
	                'success' => true,
	                'msg'     => 'El gasto ha sido agregado correctamente.'
            	));
        	} else {
				return Redirect::to('expense/index')->with('success', 'El gasto ha sido agregado correctamente.');
			}
		}

		if($isAjax) {
        	return Response::json(array (
	            'success' => false,
	            'msg'     => 'Ha ocurrido un error, tenemos algunos monos revizando el problema.'
            ));
	    } else {
	        return Redirect::back()->withInput()->withErrors($expense->errors);
	    }
	}

	//Función que se encarga de hacer la busqueda de gastos en base a la fecha de inicio y fecha final
	public function searchExpenses(){
		$min = Input::get('min');
		$max = Input::get('max');
		$expense = new Expense;
		$expenses = $expense->getMyExpensesSrch(Auth::user()->id, $min, $max);
		if(Request::ajax())
			return Response::json($expenses);	
		return 
			Response::json(array ('success' => false, 'msg' => 'Hubo un error, estamos azotando a los culpables ;)'));	
	}
}