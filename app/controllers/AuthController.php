<?php

class AuthController extends BaseController {
 
    /**
     * Attempt user login
     */
    public function doLogin(){
		//Obtener el email
		$email = mb_strtolower(trim(Input::get('email')));
		//ahora el pass
		$password = Input::get('password');
		
		//Realizar la autenticación 
		if(Auth::attempt(['email' => $email, 'password' => $password])){
    		return Redirect::to('expense/index');
    	}

    	//En caso de error 
    	return Redirect::back()->with('msg', 'Email o password incorrectos!');
    }

    public function doLogout(){
    	//Desconectamos al usuario
    	Auth::logout();
    	//Redireccionamos al inicio
    	return Redirect::to('/');
    }

}