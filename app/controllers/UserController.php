<?php 
/**Este Controlador se encarga administrar las acciones de un usuario**/
/*
Las funciones son
#addUser
*/
class UserController extends BaseController {

	public function addUser(){
		$user = new User;
		$data = Input::all();

		if ($user->isValid($data)) {
			$data['password'] = Hash::make($data['password']);
			$user->fill($data);
			$user->save();

			return Redirect::back()->with('msg_succeces', 'Te haz registrado exitosamente! ');
		}

		return Redirect::back()->withInput()->withErrors($user->errors);
	}
}
